import React, { useState } from 'react';
import arrowLeftImage from '../img/arrowLeft.png';
import book from '../img/book.png';
import add from '../img/add.png';
import all from '../img/all.png';
import active from '../img/active.png';
import inactive from '../img/inactive.png';
import order from '../img/order.png';
import user from '../img/user.png';
import settings from '../img/setting.png';
import home from '../img/home.png';
import logout from '../img/logout.png';

const AdminDashboard = () => {
  const [open, setOpen] = useState(true);
  const [selectedMenu, setSelectedMenu] = useState(0);
  const [hideText, setHideText] = useState(false);

  const handleMenuHover = (menuIndex) => {
    if (selectedMenu !== menuIndex) {
      setSelectedMenu(menuIndex);
    }
  };

  return (
    <div className="flex">
      <div className={`${open ? "w-[340px]" : "w-[120px]"} duration-300 h-screen p-3 pt-4 bg-black relative`}>
        <img
          src={arrowLeftImage}
          className={`w-8 h-8 absolute cursor-pointer rounded-full -right-4 top-[78px] transform -translate-y-1/2 border-[3px] border-black ${!open && "rotate-180"}`}
          alt="Arrow left"
          onClick={() => setOpen(!open)}
        />
        <div className="flex gap-x-5 mt-4 items-center border-b pb-4 border-gray-700">
          <img
            src={book}
            className={`cursor-pointer duration-500 w-12 h-12 ml-[20px]`}
          />
          <h1
            className={`text-white origin-left font-medium text-xl duration-300 mt-1 ${!open && 'scale-0'}`}>Admin <br/> Dashboard
          </h1>
        </div>
        <div>

          {/*Add Product*/}
          <div className="pt-[24px] pl-1 pb-[5px] -ml-[5px]">
            <div
              className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 1 ? "bg-gray-500" : ""}`}
              onMouseEnter={() => handleMenuHover(1)}
              onMouseLeave={() => handleMenuHover(0)}
            >
              <img src={add} className="w-8 h-8 ml-3" />
              <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Add
              </span>
            </div>
           </div>

          {/*All Product*/}
          <div className="pl-1 pb-[5px] -ml-[5px]">
            <div
              className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 2 ? "bg-gray-500" : ""}`}
              onMouseEnter={() => handleMenuHover(2)}
              onMouseLeave={() => handleMenuHover(0)}
            >
              <img src={all} className="w-8 h-8 ml-3" />
              <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>All
				</span>
			</div>
		   </div>

		   {/*Active Product*/}
      	  <div className="pl-1 pb-[5px] -ml-[5px]">
            <div
          className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 3 ? "bg-gray-500" : ""}`}
          onMouseEnter={() => handleMenuHover(3)}
          onMouseLeave={() => handleMenuHover(0)}
        >
          <img src={active} className="w-8 h-8 ml-3" />
          <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Active
          </span>
        </div>
      </div>

      {/*Inactive Product*/}
      <div className="pl-1 pb-[50px] -ml-[5px]">
        <div
          className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 4 ? "bg-gray-500" : ""}`}
          onMouseEnter={() => handleMenuHover(4)}
          onMouseLeave={() => handleMenuHover(0)}
        >
          <img src={inactive} className="w-8 h-8 ml-3" />
          <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Inactive
          </span>
        </div>
      </div>

      {/*Orders*/}
      <div className="pl-1 pb-[5px] -ml-[5px]">
        <div
          className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 5 ? "bg-gray-500" : ""}`}
          onMouseEnter={() => handleMenuHover(5)}
          onMouseLeave={() => handleMenuHover(0)}
        >
          <img src={order} className="w-8 h-8 ml-3" />
          <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Orders
          </span>
        </div>
      </div>

      {/*Users*/}
      <div className="pl-1 pb-[50px] -ml-[5px]">
        <div
          className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 6 ? "bg-gray-500" : ""}`}
          onMouseEnter={() => handleMenuHover(6)}
          onMouseLeave={() => handleMenuHover(0)}
        >
          <img src={user} className="w-8 h-8 ml-3" />
          <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Users
          </span>
        </div>
      </div>

      {/*Home*/}
      <div className="pl-1 pb-[5px] -ml-[5px]">
        <a href="/" className={`text-decoration-none text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 7 ? "bg-gray-500" : ""}`} onMouseEnter={() => handleMenuHover(7)} onMouseLeave={() => handleMenuHover(0)}>
          <img src={home} className="w-8 h-8 ml-3" />
          <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Home</span>
        </a>
      </div>

	  {/*Settings*/}
      <div className="pl-1 pb-[5px] -ml-[5px]">
        <div
          className={`text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 8 ? "bg-gray-500" : ""}`}
          onMouseEnter={() => handleMenuHover(8)}
          onMouseLeave={() => handleMenuHover(0)}
		>
			<img src={settings} className="w-8 h-8 ml-3" />
			<span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Settings
			</span>
		</div>
	  </div>

    {/*Logout*/}
    <div className="pl-1 pb-[5px] -ml-[5px]">
      <a href="/logout" className={`text-decoration-none text-black text-sm flex items-center gap-x-5 cursor-pointer p-3 hover:bg-gray-500 rounded-md ${selectedMenu === 9 ? "bg-gray-500" : ""}`} onMouseEnter={() => handleMenuHover(9)} onMouseLeave={() => handleMenuHover(0)}>
        <img src={logout} className="w-8 h-8 ml-3" />
        <span className={`ml-2 mr-4 text-gray-100 font-medium text-[15px] ${!open && 'hidden'} origin-left duration-200`}>Logout</span>
      </a>
    </div>

    </div>
	 </div>
	</div>
  );
}

export default AdminDashboard;