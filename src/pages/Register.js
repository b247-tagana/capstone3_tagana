import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate} from 'react-router-dom';
import UserContext from "../UserContext";

import Swal from 'sweetalert2';

export default function Register() {

	const { user } = useContext(UserContext);

	// State hooks to store values of the input fields
	const [firstName, setfirstName] = useState("");
	const [lastName, setlastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setmobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function registerUser(e){

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {

				Swal.fire({
					title: "Registration Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

				setEmail("");
				setPassword1("");
				setPassword2("");
				setfirstName("");
				setlastName("");
				setmobileNo("");
				setIsActive(false);
				navigate("/login")
			} else {

				Swal.fire({

					title: "Something went wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})

		
		alert("Thank you for registering!");
	}

	const emailExist = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({email})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){

				Swal.fire({
					title: "Duplicate email found",
					icon: 'error',
					text: 'Please provide different email!'
				})
			} else {
				registerUser()
			}
		})
	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both password match

		if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "") && (password1 === password2) && (password1.length && password2.length >= 8) && (mobileNo.length >= 11)){
			setIsActive(true);
		} 

	}, [email, password1, password2, firstName, lastName, mobileNo]);

	return (
		(user.id !== null) ?
			<Navigate to="/"/>

		:

		<Form className="container" onSubmit={emailExist}>
			<h1 className="mt-20">Register</h1>
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="firstName"
					placeholder="Enter first name"
					value = {firstName}
					onChange = {e => setfirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="lastName"
					placeholder="Enter last name"
					value = {lastName}
					onChange = {e => setlastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="mobileNumber"
					placeholder="Enter Mobile Number"
					value = {mobileNo}
					onChange = {e => setmobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password ( Minimum of 8 characters )"
					value = {password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password ( Minimum of 8 characters )"
					value = {password2}
					onChange = {e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?

			<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>

				:

			<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		</Form>
	)
}