import React, { useContext } from 'react';
import { ProductContext } from '../context/ProductContext';
import Product from '../components/Product';
import Hero from '../components/Hero';

const Home = () => {
  const { products } = useContext(ProductContext);

  const filteredProducts = products.filter((item) => {
    return (
      item.category === "Mystery/Thriller" ||
      item.category === "Science Fiction" ||
      item.category === "Drama" ||
      item.category === "Financial Literature" ||
      item.category === "Self-Help"
    );
  });

  return (
    <div>
      <Hero className="my-custom-hero" />
      <section className="py-8" id="bookHaven">
        <div>
          <h1 className="text-center mb-8 mt-[100px]">Book Haven</h1>
          <div className="text-center mb-8">
            <p className="mx-auto max-w-[1000px] text-lg tracking-wider mb-4">
              is an ecommerce bookstore, offering a wide range of books across various genres and categories. It provides a convenient and user-friendly online platform for customers to browse, search, and purchase books from the comfort of their own homes.
            </p>
          </div>
          <div className="container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-[30px]">
            {filteredProducts.map(product => {
              return (
                <Product product={product} key={product._id} />
              );
            })}
          </div>
        </div>
      </section>
      <div className="bg-black py-12 mt-10">
        <div>
          <p className="text-white text-center mt-2">Copyright &copy; Book Haven 2023. All rights reserved.</p>
        </div>
      </div>
    </div>
  );
};

export default Home;
