import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      if (typeof data.access !== "undefined") {
        localStorage.setItem('token', data.access);
        retrieveUserDetails(data.access);
        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: 'Welcome to Book Haven!'
        });
      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: 'error',
          text: 'Please check your login details and try again!'
        });
      }
    });

    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    });
  };

  useEffect(() => {
    setIsActive(email !== "" && password !== "")
  }, [email, password]);

  return (
    user.id !== null ? (
      user.isAdmin ? (
        <Navigate to="/adminDashboard" />
      ) : (
        <Navigate to="/" />
      )
    ) : (
      <div className="min-h-screen flex items-center justify-center bg-gray-100">
        <div className="bg-white w-96 px-8 py-10 rounded-md shadow-md shadow-lg">
          <h1 className="text-center text-black text-3xl font-bold mb-6">Login</h1>
          <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
              <Form.Label className="text-black">Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email here"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
                className="mb-4"
              />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Label className="text-black">Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
                className="mb-4"
              />
            </Form.Group>
            {isActive ? (
              <Button variant="primary" type="submit" id="submitBtn" className="w-full">
                Submit
              </Button>
            ) : (
              <Button variant="danger" type="submit" id="submitBtn" className="w-full" disabled>
                Submit
              </Button>
            )}
          </Form>
        </div>
      </div>
    )
  );
}
