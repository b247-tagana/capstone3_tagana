import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { IoMdArrowForward } from 'react-icons/io';
import { FiTrash2 } from 'react-icons/fi';
import CartItem from '../components/CartItem';
import { SidebarContext } from '../context/SidebarContext';
import { CartContext } from '../context/CartContext';
import Swal from 'sweetalert2';

const Sidebar = () => {

	const { isOpen, handleClose } = useContext(SidebarContext);
	const { cart, clearCart, total, itemAmount } = useContext(CartContext);

	const handleClearCart = () => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'Are you sure you want to clear your cart?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, clear it!',
			cancelButtonText: 'No, cancel',
			reverseButtons: true
		}).then((result) => {
			if (result.isConfirmed) {
				clearCart();
				Swal.fire(
					'Cart Cleared!',
					'Your cart has been cleared.',
					'success'
				);
			}
		});
	};

	return (
		<div className={`${isOpen ? 'right-0' : '-right-full'} w-full bg-white fixed top-0 h-full shadow-2xl md:w-[35vw] xl:max-w-[30vw] transition-all duration-300 z-20 px-4 lg:px-[35px]`}>
			<div className="flex items-center justify-between py-6 border-b">
				<div className="uppercase text-sm font-semibold">Shopping Cart ( {itemAmount} )</div>
				<div onClick={handleClose} className="cursor-pointer w-8 h-8 flex justify-center items-center">
					<IoMdArrowForward className="text-2xl" />
				</div>
			</div>
			<div className="flex flex-col gap-y-2 h-[620px] lg:h-[620px] overflow-y-auto overflow-x-hidden border-b">
				{cart.map(item => {
					return <CartItem item={item} key={item._id} />;
				})}
			</div>
			<div className="flex flex-col gap-y-3 py-4 mt-2">
				<div className="flex justify-between items-center">
					{/*Total: */}
					<div className="uppercase font-semibold mb-3">
						<span className="text-[17px]">Total: </span>
					</div>
					{/*(total)*/}
					<div className="uppercase font-semibold mb-3 text-right text-[17px]">
						<span className="">$ {parseFloat(total).toFixed(2)}</span>
					</div>
				</div>
				<div to="/" className="bg-black flex p-3 justify-center items-center text-white w-full font-medium text-decoration-none cursor-pointer rounded-md">Checkout
				</div>
				<div onClick={handleClearCart} className="bg-gray-400 flex p-3 justify-center items-center text-white w-full font-medium text-decoration-none cursor-pointer rounded-md">Clear Cart
				</div>
			</div>
		</div>
	);
};

export default Sidebar;
