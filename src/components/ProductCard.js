import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function ProductCard({ product, quantity, onIncrement, onDecrement }) {
  const { name, description, price, _id } = product;
  const [isBuyDisabled, setIsBuyDisabled] = useState(true); // State to manage buy button disabled/enabled state

  useEffect(() => {
    // Update isBuyDisabled state based on quantity value
    setIsBuyDisabled(quantity === 0);
  }, [quantity]);

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <div className="mt-3">
          <Card.Subtitle>Quantity</Card.Subtitle>
          <Button
            variant="outline-primary"
            onClick={() => onIncrement(_id)}
          >
            +
          </Button>
          <span className="mx-2">{quantity}</span>
          <Button
            variant="outline-primary"
            onClick={() => onDecrement(_id)}
          >
            -
          </Button>
        </div>
        <Button 
          className={`mt-2 ${isBuyDisabled ? 'bg-danger' : 'bg-primary'}`}
          as={Link}
          to={`/products/${_id}`}
          disabled={isBuyDisabled}
        >
          Buy
        </Button>
      </Card.Body>
    </Card>
  );
}

ProductCard.propTypes = {
  product: PropTypes.object.isRequired,
  quantity: PropTypes.number.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired
};
