import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { BsPlus, BsEyeFill } from 'react-icons/bs';

import { CartContext } from '../context/CartContext';
import { SidebarContext } from '../context/SidebarContext';

const Product = ({ product }) => {
  const { addToCart } = useContext(CartContext);
  const { isOpen, setIsOpen } = useContext(SidebarContext);

  const { _id, image, category, name, price, quantity } = product;
  
  return (
    <div>
      {/*image*/}
      <div className="border border-[#e4e4e4] h-[300px] mb-4 relative overflow-hidden group transition">
        <div className="w-full h-full flex justify-center items-center ">
          <div className="w-[200px] mx-auto flex justify-center items-center">
            <img className="max-h-[160px] group-hover:scale-110 transition duration-300 drop-shadow-xl" src={image} alt="" />
          </div>
          <div className="absolute top-1 right-1 group-hover:right-3 p-2 flex flex-col items-center justify-center gap-y-3 opacity-[0] group-hover:opacity-100 transition-opacity duration-300">
            <button onClick={() => addToCart(product, _id)}>
              <div className="flex justify-center items-center text-white w-8 h-8 bg-red-500">
                <BsPlus className="text-3xl" />
              </div>
            </button>
            <Link to={`/products/${_id}`} className="w-8 h-8 bg-white flex justify-center items-center text-primary drop-shadow-xl">
              <BsEyeFill />
            </Link>
          </div>
        </div>
      </div>
      {/*category, name & price*/}
      <div className="text-sm capitalize text-gray-500">{category}</div>
      <Link to={`/products/${_id}`} className="text-decoration-none text-black">
        <div className="font-bold">{name}</div>
      </Link>
      <div className="font-semibold">
      $ {price}
      </div>
    </div>
  ) 
};

export default Product;