import React, { useEffect, useState, useContext } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { BsCart } from 'react-icons/bs';
import { BsFillPersonFill } from 'react-icons/bs';
import { BsPersonFillGear } from 'react-icons/bs';
import UserContext from '../UserContext';

import book from '../img/book.png';

import { SidebarContext } from '../context/SidebarContext';
import { CartContext } from '../context/CartContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const { isOpen, setIsOpen } = useContext(SidebarContext);
  const { itemAmount } = useContext(CartContext);

  const location = useLocation();
  const shouldShowNavbar = !location.pathname.includes('/adminDashboard'); // Check if current path includes /adminDashboard

  return shouldShowNavbar ? (
    <Navbar bg="black" expand="lg" className="fixed-top">
      <Navbar.Brand className="ml-5" to="/">
        <a className="navbar-brand text-white ml-5" href="/">
          <img src={book} width="30" height="30" class="d-inline-block align-top" alt="" />
        Book Haven
        </a>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse className="mr-5" id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link
            className="text-white"
            style={{ display: user.isAdmin === true ? 'inline-flex' : 'none' }}
            as={NavLink}
            to="/adminDashboard"
          >
          <BsPersonFillGear className="text-xl text-white d-inline-block align-top mr-1" />
            Admin Dashboard
          </Nav.Link>
          {user.id !== null ? (
            <Nav.Link className="text-white" as={NavLink} to="/logout">
            <BsFillPersonFill className="text-xl text-white d-inline-block align-top mr-1" />
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link className="text-white" as={NavLink} to="/login">
              <BsFillPersonFill className="text-xl text-white d-inline-block align-top mr-1" />
                Login
              </Nav.Link>
              <Nav.Link className="text-white" as={NavLink} to="/register">
              <BsFillPersonFill className="text-xl text-white d-inline-block align-top mr-1" />
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
        <div className="ml-10 mr-5">
          <div
            onClick={() => setIsOpen(!isOpen)}
            className="cursor-pointer flex relative mr-5 max-w-[50px]" // Add necessary classes for styling
          >
            <BsCart className="text-xl text-white" />
            <div className="bg-red-500 absolute -right-2 -bottom-2 text-[12px] w-[18px] h-[18px] text-white rounded-full flex justify-center items-center">
              {itemAmount}
            </div>
          </div>
        </div>
      </Navbar.Collapse>
    </Navbar>
  ) : null;
}
