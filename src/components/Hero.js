import React from 'react';
import { Link } from 'react-router-dom';

import ImageBackground from '../img/ImageBackground.png';

const Hero = () => {
	return (
		<section className="bg-gray-100 h-[850px] bg-no-repeat bg-cover bg-center py-24">
			<div className="container mx-auto flex justify-around h-full">
				{/*text*/}
				<div className="flex flex-col justify-center">
					{/*pretitle*/}
					<div className="font-semibold flex items-center uppercase">
						<div className="w-8 h-[2px] bg-gray-500 mr-2"></div>Where Bookworms Thrive
					</div>
					{/*title*/}
					<h1 className="text-[70px] leading-[1.1] font-light mb-4 mt-5"> BOOK HAVEN, <br />
					<span className="font-semibold">YOUR LITERARY PARADISE</span>
					</h1>
					<a href="#bookHaven" className="self-start uppercase font-semibold border-b-2 border-black text-decoration-none text-black">Shop Now</a>
				</div>
			</div>
		</section>
	);
};

export default Hero;
